---
title: "Pipeline dataflow"
description: >
  Diagram of the dataflow throughout the pipeline
---

![Pipeline dataflow](pipeline-dataflow.png "Pipeline dataflow")
