---
title: "Infrastructure"
description: Infrastructure and environments
layout: inventory-table
weight: 50
---

The CKI setup uses different infrastructure environments for its services.
